package net.aunacraft.warpextension;

import com.google.common.collect.Lists;
import lombok.val;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.autocompleetion.impl.NoCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.warpextension.utils.LocationUtil;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class WarpExtension extends JavaPlugin {

    private ArrayList<String> whereAlreadyOnSpawn;

    @Override
    public void onEnable() {

        val CONSOLE_PREFIX = "[Warp] ";

        this.getLogger().info(CONSOLE_PREFIX + " Initializing plugin...");
        this.whereAlreadyOnSpawn = Lists.newArrayList();

        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, playerRegisterEvent -> {

            if (whereAlreadyOnSpawn.contains(playerRegisterEvent.getPlayer().getName())) {
                return;
            }

            if (LocationUtil.loadLocation(this, "spawn") != null) {
                playerRegisterEvent.getPlayer().toBukkitPlayer().teleport(LocationUtil.loadLocation(this, "spawn"));
                playerRegisterEvent.getPlayer().sendMessage("warp.spawn.success");
                whereAlreadyOnSpawn.add(playerRegisterEvent.getPlayer().getName());
            } else {
                playerRegisterEvent.getPlayer().sendMessage("warp.spawn.unsuccess");
            }
        });

        AunaAPI.getApi().registerCommand(
                CommandBuilder.beginCommand("warp")
                        .permission("system.warp")
                        .aliases("ort", "orte", "warps")
                        .subCommand(
                                CommandBuilder.beginCommand("set")
                                        .permission("system.warp.set")
                                        .parameter(ParameterBuilder.beginParameter("warpName").autoCompletionHandler(new NoCompletionHandler()).required().build())
                                        .aliases("setwarp")
                                        .handler((aunaPlayer, commandContext, strings) -> {
                                            LocationUtil.safeLocation(
                                                    this, aunaPlayer.toBukkitPlayer().getLocation(), commandContext.getParameterValue("warpName", String.class)
                                            );
                                            aunaPlayer.sendMessage("warp.set.succesfull");
                                        }).build()
                        )
                        .subCommand(
                                CommandBuilder.beginCommand("list")
                                        .handler((aunaPlayer, commandContext, strings) -> {
                                            try {
                                                HashMap<String, Location> locations = LocationUtil.getLocations(WarpExtension.getPlugin(WarpExtension.class));

                                                if (locations == null) {
                                                    aunaPlayer.sendMessage("warp.unsuccesfull");
                                                    return;
                                                }

                                                aunaPlayer.sendMessage("warp.list.header");

                                                locations.keySet().forEach(key -> {
                                                    val component = new TextComponent(aunaPlayer.getMessage("warp.list.content", key.replaceAll(".json", "")));
                                                    component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warp " + key.replaceAll(".json", "")));
                                                    component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(aunaPlayer.getMessage("warp.teleportclick"))));
                                                    aunaPlayer.toBukkitPlayer().spigot().sendMessage(component);
                                                });

                                            } catch (IOException e) {
                                                aunaPlayer.sendMessage("warp.unsuccesfull");
                                            }
                                        }).build()
                        )
                        .parameter(ParameterBuilder.beginParameter("warpName").autoCompletionHandler(new NoCompletionHandler()).required().build())
                        .handler((aunaPlayer, commandContext, strings) -> {
                            if (commandContext.hasParameter("warpName")) {
                                Location location = LocationUtil.loadLocation(this, commandContext.getParameterValue("warpName", String.class));

                                if (location == null) {
                                    aunaPlayer.sendMessage("warp.teleport.cancel");
                                    return;
                                }

                                aunaPlayer.toBukkitPlayer().teleport(location);
                                aunaPlayer.sendMessage("warp.teleport");
                            }
                        }).build()
        );

        super.onEnable();

        this.getLogger().info(CONSOLE_PREFIX + "Initialized " + this.getDescription().getName() + "v" + this.getDescription().getVersion() + " succesfull!");
    }
}
