package net.aunacraft.warpextension.utils;

import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.Plugin;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public class LocationUtil {

    public static String locationToString(Location location) {
        JSONObject json = new JSONObject();
        json.put("world", location.getWorld().getName());
        json.put("x", location.getX()).put("y", location.getY()).put("z", location.getZ());
        json.put("yaw", location.getYaw()).put("pitch", location.getPitch());
        return json.toString();
    }

    public static Location locationFromString(String str) {
        JSONObject json = new JSONObject(str);
        World world = getWorld(json.getString("world"));
        double x = json.getDouble("x");
        double z = json.getDouble("z");
        double y = json.getDouble("y");
        float yaw = json.getFloat("yaw");
        float pitch = json.getFloat("pitch");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static HashMap<String, Location> getLocations(Plugin plugin) throws IOException {
        HashMap<String, Location> locations = new HashMap<>();
        for (File file : new File(plugin.getDataFolder() + "/locations/").listFiles()) {
            locations.put(file.getName(), locationFromString(new String(Files.readAllBytes(file.toPath()))));
        }
        return locations;
    }

    @SneakyThrows
    public static void safeLocation(Plugin plugin, Location location, String name) {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();
        File locationsFolder = new File(plugin.getDataFolder(), "locations");
        locationsFolder.mkdir();

        File file = new File(locationsFolder, name + ".json");
        if (file.exists())
            file.delete();
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        writer.write(locationToString(location));
        writer.close();
    }

    public static Location loadLocation(Plugin plugin, String name) {

        File file = new File(plugin.getDataFolder() + "/locations/" + name + ".json");
        if (file.exists()) {
            try {
                return locationFromString(new String(Files.readAllBytes(file.toPath())));
            } catch (Exception ignored) {
                System.err.println("Cant load location!");
            }
        }
        return null;

    }

    private static World getWorld(String wordName) {
        if (!worldExists(wordName)) {
            return Bukkit.createWorld(new WorldCreator(wordName));
        } else {
            return Bukkit.getWorld(wordName);
        }
    }

    /**
     * Helps you with regions
     *
     * @return returns if the target location is in a specific region
     * @author ytendx
     */
    public static boolean isLocationInRegion(Location target, Location locA, Location locB) {
        double maxX = Math.max(locA.getX(), locB.getX());
        double minX = Math.min(locA.getX(), locB.getX());
        double maxY = Math.max(locA.getY(), locB.getY());
        double minY = Math.min(locA.getY(), locB.getY());
        double maxZ = Math.max(locA.getZ(), locB.getZ());
        double minZ = Math.min(locA.getZ(), locB.getZ());
        return target.getX() <= maxX && target.getX() >= minX &&
                target.getY() <= maxY && target.getY() >= minY &&
                target.getZ() <= maxZ && target.getZ() >= minZ;
    }

    private static boolean worldExists(String worldName) {
        for (World world : Bukkit.getWorlds()) {
            if (world.getName().equals(worldName)) return true;
        }
        return false;
    }

}
